<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ad="xml_version_1.0"
	xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml"/>

	<xsl:template match="/">
		<xsl:element name="string">
			<xsl:apply-templates select="//ad:Guest|//end:Guest"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="ad:Guest">
		<xsl:value-of select="concat(@Age,'/',@Nationalty,'/',@Gender,'/',@Name,'/',ad:Type,'/')"/>
		<xsl:value-of select="ad:Profile/ad:Address"/>
		<xsl:if test="ad:Profile/ad:Email">
			<xsl:text>/</xsl:text>
			<xsl:value-of select="ad:Profile/ad:Email"/>
		</xsl:if>
		<xsl:text>|</xsl:text>
	</xsl:template>
	
	<xsl:template match="end:Guest">
		<xsl:value-of select="concat('end:',@Age,'/',@Nationalty,'/',@Gender,'/',@Name,'/',ad:Type,'/')"/>
		<xsl:value-of select="ad:Profile/ad:Address"/>
		<xsl:if test="ad:Profile/ad:Email">
			<xsl:text>/</xsl:text>
			<xsl:value-of select="ad:Profile/ad:Email"/>
		</xsl:if>
		<xsl:text>|</xsl:text>
	</xsl:template>
</xsl:stylesheet>
